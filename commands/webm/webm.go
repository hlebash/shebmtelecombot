package webm

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pkg/errors"
	"log"
	"os"
	"regexp"
	"strings"
	"tgbot/config"
	"tgbot/util"
	"time"
)

/*
"-v", "error",
            "-threads", str(FFMPEG_THREADS),
            "-i", "pipe:0", # read input from stdin
            "-fs", "19999850", ##limit filesize
            "-map", "V:0?", # select video stream
            "-map", "0:a?", # ignore audio if doesn't exist
            "-c:v", "libx264", # specify video encoder
            "-max_muxing_queue_size", "9999", # https://trac.ffmpeg.org/ticket/6375
            "-movflags", "+faststart", # optimize for streaming
            "-preset", "slow", # https://trac.ffmpeg.org/wiki/Encode/H.264#a2.Chooseapresetandtune
            "-timelimit", "900", # prevent DoS (exit after 15 min)
            "-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2",
*/

const (
	webmType      = "webm"
	webmExtension = ".webm"
	webmMime      = "video/webm"
	webmURLRegex  = `(http.?://.*\.(?:webm))`
)

func convertWebm(filepath string) ([]byte, error) {
	return util.ConvertToMP4(filepath, webmType)
}

func processMP4(link string) ([]byte, error) {
	outputFile := fmt.Sprintf("%v.webm", time.Now().Unix())
	outputFilePath := fmt.Sprint(config.Config.Webm.SaveFolder, outputFile)
	err := util.DownloadFile(link, outputFilePath)
	if err != nil {
		return nil, errors.Wrap(err, "\nfailed to download")
	}

	err = util.CheckFileType(outputFilePath, webmMime)
	if err != nil {
		return nil, err
	}

	output, err := convertWebm(outputFilePath)
	if err != nil {
		return nil, errors.Wrap(err, "\nfailed to convert")
	}

	os.Remove(outputFilePath)

	return output, nil
}

func WebmUrlHandler(upd tgbotapi.Update, api *tgbotapi.BotAPI) {
	regex, err := regexp.Compile(webmURLRegex)
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "regexp compilation error")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "regexp compilation error")))
		api.Send(mess)
		return
	}
	url := regex.Find([]byte(upd.Message.Text))

	mess := tgbotapi.NewMessage(upd.Message.Chat.ID, "Конверчу")
	mess.ReplyToMessageID = upd.Message.MessageID
	sentMess, _ := api.Send(mess)

	output, err := processMP4(string(url))
	api.DeleteMessage(tgbotapi.NewDeleteMessage(sentMess.Chat.ID, sentMess.MessageID))
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to process")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID,
			fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to process")))
		api.Send(mess)
		return
	}

	video := tgbotapi.NewVideoUpload(upd.Message.Chat.ID, tgbotapi.FileBytes{
		Name:  "video.mp4",
		Bytes: output,
	})
	video.ReplyToMessageID = upd.Message.MessageID
	video.MimeType = "video/mp4"

	api.Send(video)
}

func WebmFileHandler(upd tgbotapi.Update, api *tgbotapi.BotAPI) {
	if !strings.HasSuffix(upd.Message.Document.FileName, webmExtension) {
		log.Println("Received not webm. Skipping...")
		return
	}

	file, err := api.GetFile(tgbotapi.FileConfig{FileID: upd.Message.Document.FileID})
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to get file")))
		api.Send(tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to get file"))))
		return
	}
	link := file.Link(api.Token)

	mess := tgbotapi.NewMessage(upd.Message.Chat.ID, "Конверчу") // TODO: придумать, как убрать дублирование кода 2
	mess.ReplyToMessageID = upd.Message.MessageID
	sentMess, _ := api.Send(mess)

	output, err := processMP4(link)
	api.DeleteMessage(tgbotapi.NewDeleteMessage(sentMess.Chat.ID, sentMess.MessageID))
	if err != nil {
		errMess := fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to process"))

		log.Println(errMess)
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, errMess)
		api.Send(mess)
		return
	}

	video := tgbotapi.NewVideoUpload(upd.Message.Chat.ID, tgbotapi.FileBytes{
		Name:  "video.mp4",
		Bytes: output,
	})
	video.ReplyToMessageID = upd.Message.MessageID
	video.MimeType = "video/mp4"

	api.Send(video)
}
