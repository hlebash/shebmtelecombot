package mov

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"tgbot/config"
	"tgbot/util"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pkg/errors"
)

const (
	movType      = "mov"
	movExtension = ".mov"
	movMime      = "video/quicktime"
	movURLRegex  = `(http.?://.*\.(?:mov))`
)

func convertMov(filepath string) ([]byte, error) {
	return util.ConvertToMP4(filepath, movType)
}

func processMP4(link string) ([]byte, error) {
	outputFile := fmt.Sprintf("%v.mov", time.Now().Unix())
	outputFilePath := fmt.Sprint(config.Config.Webm.SaveFolder, outputFile)
	err := util.DownloadFile(link, outputFilePath)
	if err != nil {
		return nil, errors.Wrap(err, "\nfailed to download")
	}

	err = util.CheckFileType(outputFilePath, movMime)
	if err != nil {
		return nil, err
	}

	output, err := convertMov(outputFilePath)
	if err != nil {
		return nil, errors.Wrap(err, "\nfailed to convert")
	}

	os.Remove(outputFilePath)

	return output, nil
}

func MovUrlHandler(upd tgbotapi.Update, api *tgbotapi.BotAPI) {
	regex, err := regexp.Compile(movURLRegex)
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "regexp compilation error")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "regexp compilation error")))
		api.Send(mess)
		return
	}
	url := regex.Find([]byte(upd.Message.Text))

	mess := tgbotapi.NewMessage(upd.Message.Chat.ID, "Конверчу")
	mess.ReplyToMessageID = upd.Message.MessageID
	sentMess, _ := api.Send(mess)

	output, err := processMP4(string(url))
	api.DeleteMessage(tgbotapi.NewDeleteMessage(sentMess.Chat.ID, sentMess.MessageID))
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to process")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID,
			fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to process")))
		api.Send(mess)
		return
	}

	video := tgbotapi.NewVideoUpload(upd.Message.Chat.ID, tgbotapi.FileBytes{
		Name:  "video.mp4",
		Bytes: output,
	})
	video.ReplyToMessageID = upd.Message.MessageID
	video.MimeType = "video/mp4"

	api.Send(video)
}

func MovFileHandler(upd tgbotapi.Update, api *tgbotapi.BotAPI) {
	if !strings.HasSuffix(upd.Message.Document.FileName, movExtension) {
		log.Println("Received not mov. Skipping...")
		return
	}

	file, err := api.GetFile(tgbotapi.FileConfig{FileID: upd.Message.Document.FileID})
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to get file")))
		api.Send(tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to get file"))))
		return
	}
	link := file.Link(api.Token)

	mess := tgbotapi.NewMessage(upd.Message.Chat.ID, "Конверчу") // TODO: придумать, как убрать дублирование кода
	mess.ReplyToMessageID = upd.Message.MessageID
	sentMess, _ := api.Send(mess)

	output, err := processMP4(link)
	api.DeleteMessage(tgbotapi.NewDeleteMessage(sentMess.Chat.ID, sentMess.MessageID))
	if err != nil {
		errMess := fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to process"))

		log.Println(errMess)
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, errMess)
		api.Send(mess)
		return
	}

	video := tgbotapi.NewVideoUpload(upd.Message.Chat.ID, tgbotapi.FileBytes{
		Name:  "video.mp4",
		Bytes: output,
	})
	video.ReplyToMessageID = upd.Message.MessageID
	video.MimeType = "video/mp4"

	api.Send(video)
}
