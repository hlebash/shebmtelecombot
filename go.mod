module tgbot

go 1.12

require (
	github.com/gabriel-vasile/mimetype v1.2.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
