package router

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"regexp"
	"tgbot/config"
)

type Router interface {
	Run(upd tgbotapi.Update, api *tgbotapi.BotAPI)
}

//--------------------------------

type commonRouter struct {
	Function func(upd tgbotapi.Update, api *tgbotapi.BotAPI)
}

func (c *commonRouter) Run(upd tgbotapi.Update, api *tgbotapi.BotAPI) {

	go c.Function(upd, api)
}

//--------------------------------

type CommandRouter struct {
	Command string
	commonRouter
}

func NewCommandRouter(command string, fun func(upd tgbotapi.Update, api *tgbotapi.BotAPI)) *CommandRouter {
	return &CommandRouter{command, commonRouter{fun}}
}

//--------------------------------

type DocumentRouter struct {
	Mime string
	commonRouter
}

func NewDocumentRouter(mime string, fun func(upd tgbotapi.Update, api *tgbotapi.BotAPI)) *DocumentRouter {
	return &DocumentRouter{mime, commonRouter{fun}}
}

//--------------------------------

type UrlRegexpRouter struct {
	Url *regexp.Regexp
	commonRouter
}

func NewUrlRegexpRouter(url *regexp.Regexp, fun func(upd tgbotapi.Update, api *tgbotapi.BotAPI)) *UrlRegexpRouter {
	return &UrlRegexpRouter{url, commonRouter{fun}}
}

//--------------------------------
type PrivateCommandRouter struct {
	Command string
	commonRouter
}

func NewPrivateCommandRouter(command string, fun func(upd tgbotapi.Update, api *tgbotapi.BotAPI)) *PrivateCommandRouter {
	return &PrivateCommandRouter{command, commonRouter{fun}}
}

func (c *PrivateCommandRouter) IsUserAllowed(id int) bool {
	for _, v := range config.Config.Bot.Developer {
		if id == v {
			return true
		}
	}
	return false
}
