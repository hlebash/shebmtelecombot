package main

import (
	"errors"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"tgbot/commands/admin"
	"tgbot/commands/mov"
	"tgbot/commands/uptime"
	"tgbot/commands/webm"
	"tgbot/config"
	"tgbot/router"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var routers = []router.Router{
	router.NewPrivateCommandRouter("amidev", admin.IsDevPrivateCommand),
	router.NewPrivateCommandRouter("shutdown", admin.ShutdownPrivateCommand),
	router.NewCommandRouter("status", admin.StatusCommand),
	router.NewUrlRegexpRouter(regexp.MustCompile(`(http.?://.*\.(?:webm))`), webm.WebmUrlHandler),
	router.NewUrlRegexpRouter(regexp.MustCompile(`(http.?://.*\.(?:mov))`), mov.MovUrlHandler),
	router.NewDocumentRouter("video/webm", webm.WebmFileHandler),
	router.NewDocumentRouter("video/quicktime", mov.MovFileHandler),
	router.NewDocumentRouter("application/octet-stream", mov.MovFileHandler),   // клиент умеет заливать мов и сам
	router.NewDocumentRouter("application/octet-stream", webm.WebmFileHandler), // Клиент айфона шлет вебмки (и скорее всего все файлы) со сломанным маймом
	router.NewCommandRouter("uptime", uptime.UptimeCommand),
}

func main() {
	if len(os.Args) > 1 {
		config.Config.Load(os.Args[1])
	} else {
		panic(errors.New("specify a config file"))
	}

	uptime.StartTime = time.Now()

	var Client http.Client
	if config.Config.Proxy.Enable {
		proxyUrl, _ := url.Parse("http://" + config.Config.Proxy.IP + ":" + config.Config.Proxy.Port) // IP прокси
		Client.Transport = &http.Transport{Proxy: http.ProxyURL(proxyUrl)}
	}

	bot, err := tgbotapi.NewBotAPI(config.Config.Bot.Apikey)
	if err != nil {
		log.Panicln("PANIC!", err)
	}
	bot.Client = &Client

	bot.Debug = config.Config.Bot.Debug
	tgbotapi.SetLogger(log.New(os.Stderr, "{BOTDEBUGMESS}", log.Ldate|log.Lmsgprefix|log.Llongfile))

	if config.Config.Logs.SaveLogs {
		f, err := os.OpenFile(config.Config.Logs.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("PANIC! Error opening file %v", err)
		}
		defer f.Close()
		log.SetOutput(f)
		tgbotapi.SetLogger(log.New(f, "{BOTDEBUGMESS}", log.Ldate|log.Lmsgprefix|log.Llongfile))
	}
	log.Printf("Authorized on account %s", bot.Self.UserName) // Авторизация бота

	u := tgbotapi.NewUpdate(-1)
	u.Timeout = 60 // Обновление лога

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		var username string
		if update.Message.From.UserName == "" {
			username = update.Message.From.FirstName + " " + update.Message.From.LastName
		} else {
			username = update.Message.From.UserName
		}
		log.Printf("[INVITE: %s]\n[USER_MESSAGE]{%s}[%s] %s", update.Message.Chat.InviteLink, update.Message.Chat.Title, username, update.Message.Text)
		for _, r := range routers {
			switch r.(type) {
			case *router.CommandRouter:
				{
					if update.Message.IsCommand() {
						if update.Message.CommandWithAt() == r.(*router.CommandRouter).Command {
							// log.Printf("Command "%s" is executed by %s\n", r.(*router.CommandRouter).Command ,update.Message.From.UserName)
							r.(*router.CommandRouter).Run(update, bot)
						}
					}
				}
			case *router.DocumentRouter:
				{
					if update.Message.Document != nil && update.Message.Document.MimeType == r.(*router.DocumentRouter).Mime {
						// log.Printf("Command "%s" is executed by %s\n", r.(*router.CommandRouter).Command ,update.Message.From.UserName)
						r.(*router.DocumentRouter).Run(update, bot)
					}
				}
			case *router.UrlRegexpRouter:
				{
					if r.(*router.UrlRegexpRouter).Url.MatchString(update.Message.Text) {
						// log.Printf("Command "%s" is executed by %s\n", r.(*router.CommandRouter).Command ,update.Message.From.UserName)
						r.(*router.UrlRegexpRouter).Run(update, bot)
					}
				}
			case *router.PrivateCommandRouter:
				{
					if update.Message.IsCommand() {
						if update.Message.Command() == r.(*router.PrivateCommandRouter).Command {
							if r.(*router.PrivateCommandRouter).IsUserAllowed(update.Message.From.ID) {
								// log.Printf("Private Command "%s" is executed by %s\n", r.(*router.PrivateCommandRouter).Command ,update.Message.From.UserName)
								r.(*router.PrivateCommandRouter).Run(update, bot)
							} else {
								mess := tgbotapi.NewMessage(update.Message.Chat.ID, "Не твоя дверь, дружок-пирожок")
								_, err := bot.Send(mess)
								if err != nil {
									log.Println("ERROR!", err)
								}
							}
						}
					}
				}
			}
		}
	}
}
