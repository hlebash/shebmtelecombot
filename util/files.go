package util

import (
	"fmt"
	"github.com/gabriel-vasile/mimetype"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"tgbot/config"
	"time"

	"github.com/pkg/errors"
)

func ConvertToMP4(filepath string, filetype string) ([]byte, error) {
	outputfile := fmt.Sprintf("%v.mp4", time.Now().Unix())
	outputFilepath := fmt.Sprint(config.Config.Webm.SaveFolder, outputfile)

	ffmpeg := exec.Command(
		"ffmpeg",
		"-threads", strconv.Itoa(config.Config.Webm.Threads),
		"-y",
		"-f", filetype,
		"-i", filepath,
		"-fs", "19999850",
		"-map", "V:0?",
		"-map", "0:a?",
		"-c:v", "libx264",
		"-max_muxing_queue_size", "9999",
		"-movflags", "+faststart",
		"-preset", "fast",

		"-crf", "28",
		"-profile:v", "high",
		"-level", "4.2",
		"-max_muxing_queue_size", "4096",
		"-movflags", "+faststart",
		"-strict", "-2",
		"-pix_fmt", "yuv420p",

		"-timelimit", "900",
		"-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2",
		"-f", "mp4",
		outputFilepath)

	ffmpeg.Stdout = os.Stdout
	ffmpeg.Stderr = os.Stdout

	if err := ffmpeg.Run(); err != nil {
		return nil, errors.Wrap(err, "\nffmpeg.Start()")
	}

	bytes, err := ioutil.ReadFile(outputFilepath)
	if err != nil {
		return nil, errors.Wrap(err, "\nioutil.ReadFile()")
	}

	err = os.Remove(outputFilepath)
	if err != nil {
		return bytes, err
	}

	return bytes, nil
}

func DownloadFile(url string, filepath string) error {
	response, err := http.Head(url)
	if err != nil {
		return err
	}
	fileSize, _ := strconv.Atoi(response.Header.Get("Content-Length"))
	if fileSize > 51380224 {
		return errors.New("file too big")
	}
	response, err = http.Get(url)
	defer func() {
		if response != nil {
			response.Body.Close()
		}
	}()
	if err != nil {
		return err
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filepath, bytes, 0644)
	if err != nil {
		return err
	}
	return nil
}

func CheckFileType(path, mime string) error {
	m, err := mimetype.DetectFile(path)
	if err != nil {
		return errors.Wrap(err, "\ndetection error")
	}
	if !m.Is(mime) {
		return errors.Wrap(err, "\nmime type checking error")
	}

	return nil
}
